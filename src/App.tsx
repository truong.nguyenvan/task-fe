import { ApolloProvider } from "@apollo/client";

import { apolloClient } from "@config/providers";
import { AppRouter } from "@config/router";

function App() {
  return (
    <ApolloProvider client={apolloClient}>
      <AppRouter />
    </ApolloProvider>
  );
}

export default App;
