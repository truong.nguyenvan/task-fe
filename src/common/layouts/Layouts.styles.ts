import styled from "@emotion/styled";
import tw from "twin.macro";

export const StyledLayout = styled.div`
  ${tw`bg-black-900 text-white h-screen flex justify-center items-center`}
`;
