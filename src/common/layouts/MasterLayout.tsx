import { Loading } from "@components/loading";
import { useAppStore } from "@store";

import { StyledLayout } from "./Layouts.styles";
interface MasterLayoutProps {
  children: React.ReactNode;
}

export const MasterLayout: React.FC<MasterLayoutProps> = ({ children }) => {
  const { loading } = useAppStore();

  return (
    <StyledLayout>
      {children}
      <Loading loading={loading} />
    </StyledLayout>
  );
};
