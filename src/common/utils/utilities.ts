export const formatHour = new Intl.NumberFormat("en-US", {
  minimumFractionDigits: 2,
});
