import styled from "@emotion/styled";
import tw from "twin.macro";

interface StyledAvatarProps {
  size: number;
}

export const StyledAvatar = styled.div<StyledAvatarProps>`
  ${tw`cursor-pointer`}

  width: ${({ size }) => size}px;
  height: ${({ size }) => size}px;

  img {
    ${tw`rounded-full`}
  }
`;
