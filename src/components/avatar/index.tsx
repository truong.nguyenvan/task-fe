import { StyledAvatar } from "./Avatar.styles";

export interface AvatarProps extends React.ImgHTMLAttributes<HTMLImageElement> {
  size?: number;
}

export const Avatar: React.FC<AvatarProps> = ({
  size = 24,
  alt,
  ...restProps
}) => {
  return (
    <StyledAvatar size={size}>
      <img alt={alt} {...restProps} />
    </StyledAvatar>
  );
};
