import styled from "@emotion/styled";
import tw from "twin.macro";

export const StyledButton = styled.button`
  ${tw`w-full text-white rounded-3xl px-8 py-2 text-lg font-semibold`}
  ${tw`hover:(text-black-900 bg-white shadow-none) transition-all duration-300 ease-in-out`}

  box-shadow: white 0px 2px 8px;
`;
