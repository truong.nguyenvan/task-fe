import styled from "@emotion/styled";
import tw from "twin.macro";

export const StyledWrapInput = styled.div`
  ${tw`relative min-h-[3.875rem]`}

  input {
    ${tw`bg-transparent text-lg w-full py-2 px-3 border-0 border-b border-white-500`}

    &::placeholder,
    &::-ms-input-placeholder {
      ${tw`text-transparent`}
    }

    &:focus {
      ${tw`outline-none`}
    }

    &:focus ~ label,
    &:not(:placeholder-shown) ~ label {
      ${tw`-top-3 text-xs text-white`}
    }

    :focus ~ .bar:before {
      ${tw`w-full`}
    }
  }

  input[type="password"] {
    letter-spacing: 0.125rem;
  }

  label {
    ${tw`absolute left-3 top-2 text-white pointer-events-none duration-300 transition-all`}
  }

  .bar {
    ${tw`relative block w-full`}

    &:before {
      ${tw`content-[""] absolute bottom-0 left-0 w-0 h-px bg-white transition-all duration-300`}
    }
  }

  .error {
    ${tw`text-xs !text-red-500 ml-3`}
  }
`;
