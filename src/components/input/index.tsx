import { StyledWrapInput } from "./Input.styles";

interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  errorMsg?: string;
}

export const Input: React.FC<InputProps> = ({
  placeholder,
  className,
  errorMsg,
  ...restProps
}) => {
  return (
    <StyledWrapInput className={className}>
      <input placeholder={placeholder} {...restProps} />
      <span className="bar"></span>
      <label>{placeholder}</label>
      {errorMsg && <span className="error">{errorMsg}</span>}
    </StyledWrapInput>
  );
};
