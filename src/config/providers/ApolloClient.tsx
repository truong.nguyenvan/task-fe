// Import necessary modules
import { ApolloClient, InMemoryCache, createHttpLink } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";

// Initialize Apollo Client
const httpLink = createHttpLink({
  uri: import.meta.env.VITE_GRAPHQL_URL,
});

// Middleware to add the bearer token to the headers
const authLink = setContext((_, { headers }) => {
  // Get the token from wherever you have stored it (e.g., local storage)
  const accessToken = localStorage.getItem("accessToken");

  // Return the headers to be used in the request
  return {
    headers: {
      ...headers,
      authorization: accessToken ? `Bearer ${accessToken}` : "", // Add token to authorization header
    },
  };
});

// Create the Apollo Client instance
export const apolloClient = new ApolloClient({
  link: authLink.concat(httpLink), // Chain the middleware with the HTTP link
  cache: new InMemoryCache(),
});
