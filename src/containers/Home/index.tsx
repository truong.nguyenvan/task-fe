import { Link } from "react-router-dom";

import { MasterLayout } from "@common/layouts";
import { Button } from "@components/button";
import { useAuth } from "@hooks";

export const Home: React.FC = () => {
  const { handleLogout } = useAuth();

  return (
    <MasterLayout>
      <div className="flex flex-col gap-8 text-center">
        <Link to="/tasks">Tasks</Link>
        <Button onClick={handleLogout}>Log out</Button>
      </div>
    </MasterLayout>
  );
};
