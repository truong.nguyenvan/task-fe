import styled from "@emotion/styled";
import tw from "twin.macro";

export const StyledLoginContainer = styled.div`
  ${tw`sm:w-[26.25rem] px-12 py-8 rounded-3xl`}

  width: calc(100% - 32px);

  box-shadow: white 0px 4px 12px;
`;
