import { Formik } from "formik";
import { object, string } from "yup";

import { MasterLayout } from "@common/layouts";
import { Button } from "@components/button";
import { Input } from "@components/input";
import { useAuth } from "@hooks";

import { StyledLoginContainer } from "./Login.styles";

interface LoginSchema {
  email: string;
  password: string;
}

const validationLoginSchema = object<LoginSchema>().shape({
  email: string()
    .email("Email must be a valid email")
    .required("Email is required"),
  password: string()
    .min(6, "Password's length must be at least 6 characters")
    .max(20, "Password can't be longer than 20 characters")
    .required("Password is required"),
});

const initialValues: LoginSchema = {
  email: "",
  password: "",
};

export const Login: React.FC = () => {
  const { handleLogin } = useAuth();

  const handleSubmit = (values: LoginSchema) => {
    handleLogin(values.email, values.password);
  };

  return (
    <MasterLayout>
      <StyledLoginContainer>
        <h1 className="mb-8 text-2xl font-bold text-center">Log In</h1>

        <Formik
          initialValues={initialValues}
          validationSchema={validationLoginSchema}
          onSubmit={handleSubmit}
        >
          {({
            touched,
            values,
            errors,
            handleChange,
            handleBlur,
            handleSubmit,
          }) => (
            <form className="flex flex-col gap-4" onSubmit={handleSubmit}>
              <Input
                name="email"
                type="email"
                placeholder="Email"
                value={values.email}
                errorMsg={touched.email ? errors.email : undefined}
                onChange={handleChange}
                onBlur={handleBlur}
              />

              <Input
                name="password"
                type="password"
                placeholder="Password"
                value={values.password}
                errorMsg={touched.password ? errors.password : undefined}
                onChange={handleChange}
                onBlur={handleBlur}
              />

              <Button type="submit">Log In</Button>
            </form>
          )}
        </Formik>
      </StyledLoginContainer>
    </MasterLayout>
  );
};
