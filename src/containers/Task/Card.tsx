import { formatHour } from "@common/utils";
import { Avatar } from "@components/avatar";

import { StyledCard, StyledTag } from "./Task.styles";
import { CardProps } from "./types";

export const Card: React.FC<CardProps> = ({ task }) => {
  return (
    <StyledCard>
      <span>{task.name}</span>

      <div className="flex flex-wrap gap-2 my-3">
        <StyledTag bgColor="chocolate">New group</StyledTag>
        <StyledTag>{formatHour.format(task.estimateHours)}</StyledTag>
      </div>

      {task.members.map(
        (member) => member.avatar && <Avatar src={member.avatar} alt="avatar" /> // TO-DO: Need to implement the Avatar component
      )}
    </StyledCard>
  );
};
