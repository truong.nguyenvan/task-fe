import { useQuery } from "@apollo/client";
import { FC, useEffect } from "react";

import { TASKS_BY_GROUP } from "@graphql/task";
import { TasksByGroupQuery, TasksByGroupQueryVariables } from "@graphql/types";

import { Card } from "./Card";
import { Drag } from "./Drag";
import { Drop } from "./Drop";
import { StyledColumn, StyledColumnTitle } from "./Task.styles";
import { ColumnProps } from "./types";

export const Column: FC<ColumnProps> = ({
  column,
  className,
  provided,
  addTasks,
}) => {
  const { data: tasks } = useQuery<
    TasksByGroupQuery,
    TasksByGroupQueryVariables
  >(TASKS_BY_GROUP, {
    variables: {
      payload: {
        taskGroupId: column.id,
      },
    },
    fetchPolicy: "no-cache",
  });

  useEffect(() => {
    if (tasks) {
      const tasksByGroup = tasks.tasksByGroup.map((task) => ({
        id: task.id,
        name: task.name,
        description: task.description,
        estimateHours: task.estimateHours,
      }));

      addTasks(column.id, tasksByGroup);
    }
  }, [addTasks, column.id, tasks]);

  return (
    <StyledColumn className={className}>
      <StyledColumnTitle {...provided?.dragHandleProps}>
        {column.name}
      </StyledColumnTitle>

      <Drop className="rounded-xl min-h-1" droppableId={column.id} type="TASK">
        {column.tasks.map((task, index) => (
          <Drag
            className="mb-2"
            draggableId={task.id}
            index={index}
            key={task.id}
          >
            <Card task={task} />
          </Drag>
        ))}
      </Drop>
    </StyledColumn>
  );
};
