import React, { FC, ReactElement } from "react";
import { Draggable, DraggableProps } from "react-beautiful-dnd";

interface DragProps extends Omit<DraggableProps, "children"> {
  className?: string;
  children: ReactElement;
  dragAll?: boolean;
}

export const Drag: FC<DragProps> = ({
  className,
  children,
  dragAll = true,
  ...props
}) => {
  return (
    <Draggable {...props}>
      {(provided) => {
        const dragHandleProps = dragAll ? provided.dragHandleProps : {};

        return (
          <div
            className={className}
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...dragHandleProps}
          >
            {React.cloneElement(children, { provided })}
          </div>
        );
      }}
    </Draggable>
  );
};
