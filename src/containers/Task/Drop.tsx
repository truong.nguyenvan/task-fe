import { FC, ReactNode } from "react";
import { Droppable, DroppableProps } from "react-beautiful-dnd";

import { StyledDragContent } from "./Task.styles";

interface DropProps extends Omit<DroppableProps, "children"> {
  children: ReactNode;
  className?: string;
}

export const Drop: FC<DropProps> = ({ children, className, ...props }) => {
  return (
    <Droppable {...props}>
      {(provided, snapshot) => (
        <StyledDragContent
          {...provided.innerRef}
          ref={provided.innerRef}
          className={className}
          isDraggingOver={snapshot.isDraggingOver}
        >
          {children}
          {provided.placeholder}
        </StyledDragContent>
      )}
    </Droppable>
  );
};
