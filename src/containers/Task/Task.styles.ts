import styled from "@emotion/styled";
import tw from "twin.macro";

interface StyledTagProps {
  bgColor?: string;
}

interface StyledDragContentProps {
  isDraggingOver: boolean;
}

//---------------------------------- Board ----------------------------------//
export const StyledBoard = styled.div`
  .column-content {
    ${tw`flex overflow-auto p-4`}

    height: calc(100vh - 4rem);
    max-width: 100vw;
  }
`;

//---------------------------------- Column ----------------------------------//
export const StyledColumn = styled.div`
  ${tw`w-80 rounded-xl p-4 shadow-lg border border-white-100`}
`;

export const StyledColumnTitle = styled.h3`
  ${tw`text-lg font-semibold mb-2`}

  div:has(&:hover) {
    ${tw`border-white-300`}
  }
`;

//---------------------------------- Card ----------------------------------//
export const StyledCard = styled.div`
  ${tw`p-4 rounded-xl border border-white-100 h-fit hover:border-white-300`}
`;

export const StyledTag = styled.div<StyledTagProps>`
  ${tw`cursor-pointer w-fit px-2 py-1 text-xs rounded-2xl`}

  background-color: ${({ bgColor }) => bgColor ?? "rgba(225,225,225,0.1)"};
`;

//---------------------------------- Drag ----------------------------------//
export const StyledDragContent = styled.div<StyledDragContentProps>`
  background-color: ${({ isDraggingOver }) =>
    isDraggingOver ? "rgba(0,0,0,0.1)" : "transparent"};
`;
