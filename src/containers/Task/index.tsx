import { useMutation, useQuery } from "@apollo/client";
import React, { useEffect } from "react";
import {
  DragDropContext,
  DraggableLocation,
  DropResult,
} from "react-beautiful-dnd";

import { MasterLayout } from "@common/layouts";
import { UPDATE_TASK } from "@graphql/task";
import { TASK_GROUPS } from "@graphql/taskGroup";
import {
  TaskGroupsQuery,
  TaskGroupsQueryVariables,
  UpdateTaskMutation,
  UpdateTaskMutationVariables,
} from "@graphql/types";
import { IColumn, useBoardStore } from "@store/board";

import { Column } from "./Column";
import { Drag } from "./Drag";
import { Drop } from "./Drop";
import { StyledBoard } from "./Task.styles";

export const Task: React.FC = () => {
  const { columns, updateBoard, addTasks } = useBoardStore();

  const { data: groups } = useQuery<TaskGroupsQuery, TaskGroupsQueryVariables>(
    TASK_GROUPS
  );
  const [updateTask] = useMutation<
    UpdateTaskMutation,
    UpdateTaskMutationVariables
  >(UPDATE_TASK);

  const isKeeping = (
    source: DraggableLocation,
    destination: DraggableLocation
  ): boolean =>
    destination.droppableId === source.droppableId &&
    destination.index === source.index;

  const updateTasks = (
    data: IColumn[],
    { droppableId: sourceDroppableId, index: sourceIndex }: DraggableLocation,
    {
      droppableId: destinationDroppableId,
      index: destinationIndex,
    }: DraggableLocation
  ): IColumn[] => {
    const dataCloned = [...data];
    const sourceColumnIdx = dataCloned.findIndex(
      (column) => column.id === sourceDroppableId
    );
    const destinationColumnIdx = dataCloned.findIndex(
      (column) => column.id === destinationDroppableId
    );
    const sourceItem = dataCloned[sourceColumnIdx].tasks[sourceIndex];

    dataCloned[sourceColumnIdx].tasks.splice(sourceIndex, 1);
    dataCloned[destinationColumnIdx].tasks.splice(
      destinationIndex,
      0,
      sourceItem
    );

    // Update task group
    updateTask({
      variables: {
        payload: {
          ...sourceItem,
          taskGroupId: destinationDroppableId,
        },
      },
    });

    return dataCloned;
  };

  const updateColumns = (
    data: IColumn[],
    { index: sourceIndex }: DraggableLocation,
    { index: destinationIndex }: DraggableLocation
  ): IColumn[] => {
    const dataCloned = [...data];
    const sourceItem = dataCloned[sourceIndex];

    dataCloned.splice(sourceIndex, 1);
    dataCloned.splice(destinationIndex, 0, sourceItem);

    return dataCloned;
  };

  const onDragEnd = (result: DropResult) => {
    const { type, source, destination } = result;

    if (!destination || isKeeping(source, destination)) return;

    if (type === "TASK") {
      const newBoard = updateTasks(columns, source, destination);
      updateBoard(newBoard);
    }

    if (type === "COLUMN") {
      const newBoard = updateColumns(columns, source, destination);
      updateBoard(newBoard);
    }
  };

  useEffect(() => {
    if (groups) {
      updateBoard(
        groups.taskGroups.map(({ id, name }) => ({ id, name, tasks: [] }))
      );
    }
  }, [groups, updateBoard]);

  return (
    <MasterLayout>
      <StyledBoard>
        <DragDropContext onDragEnd={onDragEnd}>
          <Drop
            className="column-content"
            droppableId="all-columns"
            type="COLUMN"
            direction="horizontal"
          >
            {columns.map((column, index) => {
              return (
                <Drag
                  className="mr-4"
                  key={column.id}
                  draggableId={column.id}
                  index={index}
                  dragAll={false}
                >
                  <Column column={column} addTasks={addTasks} />
                </Drag>
              );
            })}
          </Drop>
        </DragDropContext>
      </StyledBoard>
    </MasterLayout>
  );
};
