import { DraggableProvided } from "react-beautiful-dnd";

import { IColumn, ITask } from "@store/board";

export interface ColumnProps {
  column: IColumn;
  className?: string;
  provided?: DraggableProvided;
  addTasks: (columnId: string, tasks: IColumn["tasks"]) => void;
}

export interface CardProps {
  task: ITask;
}
