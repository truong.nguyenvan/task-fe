import { gql } from "@apollo/client";

export const LOGIN = gql`
  mutation Login($payload: LoginInput!) {
    login(payload: $payload) {
      accessToken
      refreshToken
    }
  }
`;
