import { gql } from "@apollo/client";

export const TASKS = gql`
  query Tasks {
    tasks {
      id
      createdAt
      updatedAt

      name
      description
      estimateHours
      status
      members {
        id
        firstName
        lastName
        avatar
      }
    }
  }
`;

export const TASKS_BY_GROUP = gql`
  query TasksByGroup($payload: FindTasksArgs!) {
    tasksByGroup(payload: $payload) {
      id
      createdAt
      updatedAt

      name
      description
      estimateHours
      status
      members {
        id
        firstName
        lastName
        avatar
      }
    }
  }
`;

export const CREATE_TASK = gql`
  mutation CreateTask($payload: CreateTaskInput!) {
    createTask(payload: $payload) {
      id
      createdAt
      updatedAt

      name
      description
      estimateHours
      status
      members {
        id
        firstName
        lastName
        avatar
      }
    }
  }
`;

export const UPDATE_TASK = gql`
  mutation UpdateTask($payload: UpdateTaskInput!) {
    updateTask(payload: $payload) {
      id
      createdAt
      updatedAt

      name
      description
      estimateHours
      status
      members {
        id
        firstName
        lastName
        avatar
      }
    }
  }
`;

export const DELETE_TASK = gql`
  mutation DeleteTask($id: String!) {
    deleteTask(id: $id)
  }
`;
