import { gql } from "@apollo/client";

export const TASK_GROUPS = gql`
  query TaskGroups {
    taskGroups {
      id
      createdAt
      updatedAt

      name
    }
  }
`;

export const CREATE_TASK_GROUP = gql`
  mutation CreateTaskGroup($payload: CreateTaskGroupInput!) {
    createTaskGroup(payload: $payload) {
      id
      createdAt
      updatedAt

      name
    }
  }
`;

export const UPDATE_TASK_GROUP = gql`
  mutation UpdateTaskGroup($payload: UpdateTaskGroupInput!) {
    updateTaskGroup(payload: $payload) {
      id
      createdAt
      updatedAt

      name
    }
  }
`;

export const DELETE_TASK_GROUP = gql`
  mutation DeleteTaskGroup($id: String!) {
    deleteTaskGroup(id: $id)
  }
`;
