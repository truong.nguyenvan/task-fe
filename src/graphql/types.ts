export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  DateTime: { input: Date; output: Date; }
};

export type Auth = {
  __typename?: 'Auth';
  accessToken: Scalars['String']['output'];
  refreshToken: Scalars['String']['output'];
};

export type CreateTaskGroupInput = {
  name: Scalars['String']['input'];
};

export type CreateTaskInput = {
  description?: InputMaybe<Scalars['String']['input']>;
  estimateHours?: Scalars['Float']['input'];
  name: Scalars['String']['input'];
  status?: InputMaybe<Task_Status>;
  taskGroupId: Scalars['String']['input'];
};

export type CreateUserInput = {
  email: Scalars['String']['input'];
  firstName: Scalars['String']['input'];
  lastName: Scalars['String']['input'];
  password: Scalars['String']['input'];
  role?: InputMaybe<Role>;
};

export type FindTaskGroupArgs = {
  id: Scalars['String']['input'];
};

export type FindTasksArgs = {
  taskGroupId: Scalars['String']['input'];
};

export type LoginInput = {
  email: Scalars['String']['input'];
  password: Scalars['String']['input'];
};

export type Mutation = {
  __typename?: 'Mutation';
  createTask: Task;
  createTaskGroup: TaskGroup;
  createUser: User;
  deleteTask: Scalars['Boolean']['output'];
  deleteTaskGroup: Scalars['Boolean']['output'];
  deleteUser: Scalars['Boolean']['output'];
  login: Auth;
  updateTask: Task;
  updateTaskGroup: TaskGroup;
  updateUser: User;
};


export type MutationCreateTaskArgs = {
  payload: CreateTaskInput;
};


export type MutationCreateTaskGroupArgs = {
  payload: CreateTaskGroupInput;
};


export type MutationCreateUserArgs = {
  payload: CreateUserInput;
};


export type MutationDeleteTaskArgs = {
  id: Scalars['String']['input'];
};


export type MutationDeleteTaskGroupArgs = {
  id: Scalars['String']['input'];
};


export type MutationDeleteUserArgs = {
  id: Scalars['String']['input'];
};


export type MutationLoginArgs = {
  payload: LoginInput;
};


export type MutationUpdateTaskArgs = {
  payload: UpdateTaskInput;
};


export type MutationUpdateTaskGroupArgs = {
  payload: UpdateTaskGroupInput;
};


export type MutationUpdateUserArgs = {
  payload: UpdateUserInput;
};

export type Query = {
  __typename?: 'Query';
  me: User;
  taskGroup: TaskGroup;
  taskGroups: Array<TaskGroup>;
  tasks: Array<Task>;
  tasksByGroup: Array<Task>;
  user: User;
  users: Array<User>;
};


export type QueryTaskGroupArgs = {
  payload: FindTaskGroupArgs;
};


export type QueryTasksByGroupArgs = {
  payload: FindTasksArgs;
};


export type QueryUserArgs = {
  id: Scalars['String']['input'];
};

export enum Role {
  Admin = 'ADMIN',
  User = 'USER'
}

export type Subscription = {
  __typename?: 'Subscription';
  onCreateTask: Task;
  onUnCompleteTasks: Array<Task>;
  onUpdateStatusTask: Task;
};

export enum Task_Status {
  Completed = 'COMPLETED',
  Pending = 'PENDING',
  Todo = 'TODO',
  UnCompleted = 'UN_COMPLETED'
}

export type Task = {
  __typename?: 'Task';
  createdAt: Scalars['DateTime']['output'];
  createdBy: Scalars['String']['output'];
  description?: Maybe<Scalars['String']['output']>;
  estimateHours: Scalars['Float']['output'];
  id: Scalars['ID']['output'];
  members: Array<User>;
  name: Scalars['String']['output'];
  status: Task_Status;
  taskGroup: TaskGroup;
  updatedAt: Scalars['DateTime']['output'];
  updatedBy: Scalars['String']['output'];
};

export type TaskGroup = {
  __typename?: 'TaskGroup';
  createdAt: Scalars['DateTime']['output'];
  createdBy: Scalars['String']['output'];
  id: Scalars['ID']['output'];
  name: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
  updatedBy: Scalars['String']['output'];
};

export type UpdateTaskGroupInput = {
  id: Scalars['String']['input'];
  name: Scalars['String']['input'];
};

export type UpdateTaskInput = {
  description?: InputMaybe<Scalars['String']['input']>;
  estimateHours?: Scalars['Float']['input'];
  id: Scalars['String']['input'];
  name: Scalars['String']['input'];
  status?: InputMaybe<Task_Status>;
  taskGroupId: Scalars['String']['input'];
};

export type UpdateUserInput = {
  firstName: Scalars['String']['input'];
  id: Scalars['String']['input'];
  lastName: Scalars['String']['input'];
  password: Scalars['String']['input'];
  role?: InputMaybe<Role>;
};

export type User = {
  __typename?: 'User';
  avatar?: Maybe<Scalars['String']['output']>;
  createdAt: Scalars['DateTime']['output'];
  email: Scalars['String']['output'];
  firstName: Scalars['String']['output'];
  id: Scalars['ID']['output'];
  lastName: Scalars['String']['output'];
  role: Role;
  updatedAt: Scalars['DateTime']['output'];
};

export type LoginMutationVariables = Exact<{
  payload: LoginInput;
}>;


export type LoginMutation = { __typename?: 'Mutation', login: { __typename?: 'Auth', accessToken: string, refreshToken: string } };

export type TasksQueryVariables = Exact<{ [key: string]: never; }>;


export type TasksQuery = { __typename?: 'Query', tasks: Array<{ __typename?: 'Task', id: string, createdAt: Date, updatedAt: Date, name: string, description?: string | null, estimateHours: number, status: Task_Status, members: Array<{ __typename?: 'User', id: string, firstName: string, lastName: string, avatar?: string | null }> }> };

export type TasksByGroupQueryVariables = Exact<{
  payload: FindTasksArgs;
}>;


export type TasksByGroupQuery = { __typename?: 'Query', tasksByGroup: Array<{ __typename?: 'Task', id: string, createdAt: Date, updatedAt: Date, name: string, description?: string | null, estimateHours: number, status: Task_Status, members: Array<{ __typename?: 'User', id: string, firstName: string, lastName: string, avatar?: string | null }> }> };

export type CreateTaskMutationVariables = Exact<{
  payload: CreateTaskInput;
}>;


export type CreateTaskMutation = { __typename?: 'Mutation', createTask: { __typename?: 'Task', id: string, createdAt: Date, updatedAt: Date, name: string, description?: string | null, estimateHours: number, status: Task_Status, members: Array<{ __typename?: 'User', id: string, firstName: string, lastName: string, avatar?: string | null }> } };

export type UpdateTaskMutationVariables = Exact<{
  payload: UpdateTaskInput;
}>;


export type UpdateTaskMutation = { __typename?: 'Mutation', updateTask: { __typename?: 'Task', id: string, createdAt: Date, updatedAt: Date, name: string, description?: string | null, estimateHours: number, status: Task_Status, members: Array<{ __typename?: 'User', id: string, firstName: string, lastName: string, avatar?: string | null }> } };

export type DeleteTaskMutationVariables = Exact<{
  id: Scalars['String']['input'];
}>;


export type DeleteTaskMutation = { __typename?: 'Mutation', deleteTask: boolean };

export type TaskGroupsQueryVariables = Exact<{ [key: string]: never; }>;


export type TaskGroupsQuery = { __typename?: 'Query', taskGroups: Array<{ __typename?: 'TaskGroup', id: string, createdAt: Date, updatedAt: Date, name: string }> };

export type CreateTaskGroupMutationVariables = Exact<{
  payload: CreateTaskGroupInput;
}>;


export type CreateTaskGroupMutation = { __typename?: 'Mutation', createTaskGroup: { __typename?: 'TaskGroup', id: string, createdAt: Date, updatedAt: Date, name: string } };

export type UpdateTaskGroupMutationVariables = Exact<{
  payload: UpdateTaskGroupInput;
}>;


export type UpdateTaskGroupMutation = { __typename?: 'Mutation', updateTaskGroup: { __typename?: 'TaskGroup', id: string, createdAt: Date, updatedAt: Date, name: string } };

export type DeleteTaskGroupMutationVariables = Exact<{
  id: Scalars['String']['input'];
}>;


export type DeleteTaskGroupMutation = { __typename?: 'Mutation', deleteTaskGroup: boolean };

export type UsersQueryVariables = Exact<{ [key: string]: never; }>;


export type UsersQuery = { __typename?: 'Query', users: Array<{ __typename?: 'User', id: string, createdAt: Date, updatedAt: Date, firstName: string, lastName: string, email: string, role: Role }> };

export type CreateUserMutationVariables = Exact<{
  payload: CreateUserInput;
}>;


export type CreateUserMutation = { __typename?: 'Mutation', createUser: { __typename?: 'User', id: string, createdAt: Date, updatedAt: Date, firstName: string, lastName: string, email: string, role: Role } };

export type UpdateUserMutationVariables = Exact<{
  payload: UpdateUserInput;
}>;


export type UpdateUserMutation = { __typename?: 'Mutation', updateUser: { __typename?: 'User', id: string, createdAt: Date, updatedAt: Date, firstName: string, lastName: string, email: string, role: Role } };

export type DeleteUserMutationVariables = Exact<{
  id: Scalars['String']['input'];
}>;


export type DeleteUserMutation = { __typename?: 'Mutation', deleteUser: boolean };

export type MeQueryVariables = Exact<{ [key: string]: never; }>;


export type MeQuery = { __typename?: 'Query', me: { __typename?: 'User', id: string, createdAt: Date, updatedAt: Date, firstName: string, lastName: string, email: string, role: Role } };
