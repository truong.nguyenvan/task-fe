import { gql } from "@apollo/client";

export const USERS = gql`
  query Users {
    users {
      id
      createdAt
      updatedAt

      firstName
      lastName
      email
      role
    }
  }
`;

export const CREATE_USER = gql`
  mutation CreateUser($payload: CreateUserInput!) {
    createUser(payload: $payload) {
      id
      createdAt
      updatedAt

      firstName
      lastName
      email
      role
    }
  }
`;

export const UPDATE_USER = gql`
  mutation UpdateUser($payload: UpdateUserInput!) {
    updateUser(payload: $payload) {
      id
      createdAt
      updatedAt

      firstName
      lastName
      email
      role
    }
  }
`;

export const DELETE_USER = gql`
  mutation DeleteUser($id: String!) {
    deleteUser(id: $id)
  }
`;

export const ME = gql`
  query Me {
    me {
      id
      createdAt
      updatedAt

      firstName
      lastName
      email
      role
    }
  }
`;
