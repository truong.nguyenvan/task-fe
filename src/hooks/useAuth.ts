import { useMutation, useQuery } from "@apollo/client";
import { useCallback, useEffect } from "react";
import { useNavigate } from "react-router-dom";

import { LOGIN } from "@graphql/auth";
import {
  LoginMutation,
  LoginMutationVariables,
  MeQuery,
  MeQueryVariables,
} from "@graphql/types";
import { ME } from "@graphql/user";
import { useAppStore, useAuthStore } from "@store";

// LocalStorage
const setAccessToken = (accessToken: string) =>
  localStorage.setItem("accessToken", accessToken);
const getAccessToken = () => localStorage.getItem("accessToken");
const removeAccessToken = () => localStorage.removeItem("accessToken");

export const useAuth = () => {
  const { openLoading, closeLoading } = useAppStore();
  const { isAuthenticated, loggedIn, loggedOut } = useAuthStore();

  const [login] = useMutation<LoginMutation, LoginMutationVariables>(LOGIN);
  const { data, error } = useQuery<MeQuery, MeQueryVariables>(ME, {
    skip: !getAccessToken(),
  });

  const navigate = useNavigate();

  // Callbacks
  const onLoggedIn = useCallback(
    (accessToken: string) => {
      // Set access token to local storage
      setAccessToken(accessToken);

      loggedIn();
      navigate("/");
    },
    [loggedIn, navigate]
  );

  const onLoggedOut = useCallback(() => {
    // Remove access token from local storage
    removeAccessToken();

    loggedOut();
    navigate("/login");
  }, [loggedOut, navigate]);

  // Handlers
  const handleLogin = async (email: string, password: string) => {
    try {
      openLoading();

      const response = await login({
        variables: {
          payload: {
            email,
            password,
          },
        },
      });

      if (response.data) onLoggedIn(response.data.login.accessToken);
    } catch (error) {
      //
    } finally {
      closeLoading();
    }
  };

  const handleLogout = () => {
    loggedOut();
  };

  useEffect(() => {
    if (data) loggedIn();
  }, [data, loggedIn]);

  useEffect(() => {
    const accessToken = getAccessToken();

    if (error || !accessToken) onLoggedOut();
  }, [error, onLoggedOut]);

  return {
    isAuthenticated,
    handleLogin,
    handleLogout,
  };
};
