import { create } from "zustand";

import { AppActions, AppState } from "./types";

const initialState: AppState = {
  loading: false,
};

export const useAppStore = create<AppState & AppActions>((set) => ({
  ...initialState,

  openLoading: () => set({ loading: true }),
  closeLoading: () => set({ loading: false }),
}));
