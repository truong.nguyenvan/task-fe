export interface AppState {
  loading: boolean;
}

export interface AppActions {
  openLoading: () => void;
  closeLoading: () => void;
}
