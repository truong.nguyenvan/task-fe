import { create } from "zustand";

import { AppActions, AuthState } from "./types";

const initialState: AuthState = {
  isAuthenticated: true,
};

export const useAuthStore = create<AuthState & AppActions>((set) => ({
  ...initialState,

  loggedIn: () => set({ isAuthenticated: true }),
  loggedOut: () => set({ isAuthenticated: false }),
}));
