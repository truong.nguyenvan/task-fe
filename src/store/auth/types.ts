export interface AuthState {
  isAuthenticated: boolean;
}

export interface AppActions {
  loggedIn: () => void;
  loggedOut: () => void;
}
