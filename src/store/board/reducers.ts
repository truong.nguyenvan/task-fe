import { BoardState, IColumn } from "./types";

export const addColumnReducer = (state: BoardState, column: IColumn) => {
  return {
    ...state,
    columns: [...state.columns, column],
  };
};

export const addTasksReducer = (
  state: BoardState,
  columnId: string,
  tasks: IColumn["tasks"]
) => {
  return {
    ...state,
    columns: state.columns.map((column) => {
      if (column.id === columnId) {
        return {
          ...column,
          tasks: [...column.tasks, ...tasks],
        };
      }
      return column;
    }),
  };
};
