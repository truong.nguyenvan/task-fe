import { create } from "zustand";

import { addColumnReducer, addTasksReducer } from "./reducers";
import { BoardActions, BoardState } from "./types";

const initialState: BoardState = {
  columns: [],
};

export const useBoardStore = create<BoardState & BoardActions>((set) => ({
  ...initialState,

  addColumn: (column) => set((state) => addColumnReducer(state, column)),
  addTasks: (columnId, tasks) =>
    set((state) => addTasksReducer(state, columnId, tasks)),
  updateBoard: (columns) => set({ columns }),
}));
