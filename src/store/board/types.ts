import { Task, TaskGroup } from "@graphql/types";

export type ITask = Pick<
  Task,
  "id" | "name" | "members" | "estimateHours" | "description"
>;

export type IColumn = Pick<TaskGroup, "id" | "name"> & {
  tasks: ITask[];
};

export interface BoardState {
  columns: IColumn[];
}

export interface BoardActions {
  addColumn: (column: IColumn) => void; // column is the group to be added
  addTasks: (columnId: string, task: ITask[]) => void; // columnId is the id of the group where the tasks will be added
  updateBoard: (columns: IColumn[]) => void;
}
